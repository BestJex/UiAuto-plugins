import requests
import getpass
import json
import os
import sys
import zipfile
from requests_toolbelt.multipart.encoder import MultipartEncoder

current_dir = os.path.split(os.path.realpath(__file__))[0]
temp_dir = '%s/temp' % current_dir
login_result = None


def spider_login(username, password):
    global login_result

    login_url = 'http://rpa-api.legion-tech.net/api/v1/users/login/password'

    headers = {
        'Accept': 'application/json, text/plain, */*'
    }

    params = {
        "username": username,
        "password": password
    }

    res = requests.post(url=login_url, data=params, headers=headers)
 
    login_result = json.loads(res.content)

    return login_result


def select_menu():
    os.system('clear')
    print('当前目录：%s' % current_dir)
    print('[a]上传每个插件的最新版本')
    print('[s]上传指定版本的插件')
    print('[e]退出程序')
    menu = input('菜单选择：')
    if menu == 'a':
        upload_all_plugin()
    elif menu == 's':
        upload_result = publish_single_plugin()
        if upload_result is False:
            menu = select_menu()
    elif menu == 'e':
        pass
    else:
        menu = select_menu()
    
    return menu


def read_dir():
    items = os.listdir('./')
    for item in items:
        if os.path.isdir(item):
            childitems = os.listdir('%s/%s' % (current_dir, item))
            print(childitems)
    print(items)


def upload_all_plugin():
    items = os.listdir('./')
    print(items)
    for item in items:
        if item != '.git' and item != 'temp' and os.path.isdir('./%s' % item):
            print('开始上传插件【%s】' % item)
            childitems = os.listdir('./%s' % item)
            if len(childitems) == 0:
                print('插件【%s】目录没有版本')
            else:
                latest_plugin_version = max(childitems)

                target_plugin_dir = './%s/%s' % (item, latest_plugin_version)
                os.system('cp -R %s/ %s/%s' % (target_plugin_dir, temp_dir, item))
                os.system('cd ./temp && zip -q -r -o %s.zip %s && cd ../' % (item, item))

                upload_plugin(plugin_name=item)


def publish_single_plugin():
    upload_result = False
    plugin_name = input('输入插件名称：')
    if not os.path.exists('%s/%s' % (current_dir, plugin_name)):
        sys.stderr.write('\033[5;37;41m插件【%s】不存在\033[0m\n' % plugin_name)
        input('按回车键继续...')
        return upload_result

    plugin_version = input('输入需要上传的版本号：')

    if not os.path.exists('./%s/%s' % (plugin_name, plugin_version)):
        sys.stderr.write('\033[5;37;41m插件版本【%s】不存在\033[0m\n' % plugin_version)
        input('按回车键继续...')
        return upload_result

    target_plugin_dir = './%s/%s' % (plugin_name, plugin_version)

    items = os.listdir(target_plugin_dir)

    if len(items) == 0:
        sys.stderr.write('\033[5;37;41m插件【%s】目录为空\033[0m\n' % plugin_version)
        input('按回车键继续...')
        return upload_result

    os.system('cp -R %s/ %s/%s' % (target_plugin_dir, temp_dir, plugin_name))

    os.system('cd ./temp && zip -q -r -o %s.zip %s && cd ../' % (plugin_name, plugin_name))

    upload_plugin(plugin_name=plugin_name)


def upload_plugin(plugin_name):
    global login_result

    upload_url = 'http://rpa-api.legion-tech.net/api/v1/plugins/upload'

    headers = {
        "Authorization": "bearer " + login_result['data']['token'],
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0', 
        'Referer': upload_url 
    }

    file = '%s/%s.zip' % (temp_dir, plugin_name)

    multipart_encoder = MultipartEncoder(
    fields = {
            #这里根据服务器需要的参数格式进行修改
            'params': json.dumps({
                'private': 0,
                'md5': ''
            }),
            'file': ('%s.zip' % plugin_name, open(file, 'rb'), 'application/zip')
        }
    )
    headers['Content-Type'] = multipart_encoder.content_type

    res = requests.post(upload_url, data=multipart_encoder, headers=headers)
    upload_result = json.loads(res.content)

    print(upload_result)
    if upload_result['isSuccess'] is True:
        print('插件【%s】上传成功' % plugin_name)
    else:
        print('插件【%s】上传失败' % plugin_name)


def main(username):
    password = getpass.getpass('请输入您的登录密码：')
    
    login_result = spider_login(username=username, password=password)

    if login_result['isSuccess'] is True:
        print('登录成功')
    else:
        print('登录失败')
        main(username=username)


if __name__ == "__main__":
    print('当前目录：%s' % current_dir)
    username = input('请输入您的用户名：')
    main(username=username)
    
    while True:
        not os.path.exists('%s/temp' % current_dir) and os.mkdir('%s/temp' % current_dir)
        selected_menu = select_menu()

        if selected_menu == 'e':
            os.system('rm -rf %s' % temp_dir)
            sys.exit(0)
        input('按回车键继续...')

