import os
import bs4
import smtplib
from email.mime.text import MIMEText
from email.header import Header, decode_header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import poplib
from email.parser import Parser  # 解析模块
from email.utils import parseaddr, formataddr  # 用于格式化邮件信息
import time
import chardet
import json


def send_mail(params):
    try:
        to = str(params['toMail'])
        title = str(params['title'])
        content = str(params['content'])
        port = int(params['port'])
        port_ssl = int(params['port_ssl'])
        ssl = params['ssl']
        # addresser = params['addresser']
        server = ''
        if params['server'] == 'qq':
            server = "smtp.qq.com"
        elif params['server'] == 'gmail':
            server = 'smtp.gmail.com'
        elif params['server'] == '126':
            server = "smtp.126.com"
        elif params['server'] == '163':
            server = 'smtp.163.com'

        # if addresser == 'customize':
        account = str(params['account'])
        password = str(params['password'])

        message = MIMEText(content, 'plain', 'utf-8')
        m = MIMEMultipart()
        m.attach(message)
        if params['file'] != "":
            if bool(params['reset']) is False:
                mailFile = params['file']
                *_, filename = os.path.split(mailFile)
                sendfile = MIMEApplication(open(mailFile, 'rb').read())
                sendfile.add_header('Content-Disposition', 'attachment', filename=filename)
                m.attach(sendfile)

        if params['files'] != '':
            if bool(params['resets']) is False:
                filespath = params['files']
                if os.path.exists(filespath):
                    for file in os.listdir(filespath):
                        if os.path.isfile(filespath + "\\" + file):
                            sendfiles = MIMEApplication(open(filespath + "\\" + file, 'rb').read())
                            sendfiles.add_header('Content-Disposition', 'attachment', filename=file)
                            m.attach(sendfiles)

        m['From'] = _format_addr(account.split('@')[0] + '<%s>' % account)
        m['To'] = _format_addr(to.split('@')[0] + '<%s>' % to)
        m['Subject'] = Header(title, 'utf-8').encode()

        smtpObj = None
        if 'no' == ssl:
            smtpObj = smtplib.SMTP()
        elif 'yes' == ssl:
            smtpObj = smtplib.SMTP_SSL(host=server)

        smtpObj.connect(server, port=port if ssl == 'no' else port_ssl)
        smtpObj.login(account, password)
        smtpObj.sendmail(account, [to], m.as_string())
        smtpObj.quit()
    except Exception as e:
        raise e
    return "邮件发送成功"


def connectMail(params):
    port = int(params['port'])
    port_ssl = int(params['port_ssl'])
    ssl = params['ssl']
    # addresser = params['addresser']
    server = ''
    if params['server'] == 'qq':
        server = "pop.qq.com"
    elif params['server'] == 'gmail':
        server = 'pop.gmail.com'
    elif params['server'] == '126':
        server = "pop.126.com"
    elif params['server'] == '163':
        server = 'pop.163.com'

    # if addresser == 'customize':
    account = params['account']
    password = params['password']

    popObj = None
    try:
        if ssl == 'no':
            popObj = poplib.POP3(server, port=port if ssl == 'no' else port_ssl)
        elif ssl == 'yes':
            popObj = poplib.POP3_SSL(server, port=port if ssl == 'no' else port_ssl)

        popObj.user(account)
        popObj.pass_(password)

        print("连接邮箱成功")
        print(popObj.getwelcome())

        return popObj
    except Exception as e:
        raise e


def getTitle(params):
    num = int(params['num'])
    popObj = params['obj']

    try:
        *_, mails, a = popObj.list()
        count = len(mails)
        print("邮件数量：", len(mails))
        if num > count:
            return "邮件序号错误"

        # for m in range(len(mails)):
        resp, lines, octets = popObj.retr(count - num + 1)
        # resp, lines, octets = popObj.retr(m+1)

        msg_content = b'\r\n'.join(lines).decode()

        msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象

        title = msg.get('Subject', '')
        if title:
            title = decode_str(title)
        # print(title)

        return title
    except Exception as e:
        raise e


def getContent(params):
    num = int(params['num'])
    popObj = params['obj']
    isFilter = False if params['filter'] == 'no' else True
    try:
        *_, mails, a = popObj.list()
        count = len(mails)
        if num > count:
            return "邮件序号错误"
        resp, lines, octets = popObj.retr(count - num + 1)
        msg_content = b'\r\n'.join(lines).decode('utf-8')
        content = ''
        msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
        # print(msg)
        for part in msg.walk():
            content_type = part.get_content_type()
            # print(content_type)
            charset = guess_charset(part)
            # 如果有附件，则直接跳过
            if part.get_filename() is not None:
                continue
            email_content_type = ''
            if charset:
                print("guess_charset::", charset)
                if 'utf-8' in charset:
                    charset = 'utf-8'
                if content_type == 'text/plain':
                    email_content_type = 'text'
                    content = part.get_payload(decode=True).decode(charset, errors='ignore')
                elif content_type == 'text/html':
                    print("text/html===============")
                    email_content_type = 'html'
                    content = part.get_payload(decode=True).decode(charset, errors='ignore')
                    # print(content)
                    if isFilter:
                        bs = bs4.BeautifulSoup(content, "html.parser")
                        [b.extract() for b in bs(['script', 'style'])]
                        content = bs.text.lstrip().rstrip().encode(charset, errors='ignore')  # .decode()
                        charset = chardet.detect(content)['encoding']
                        print("编码：", chardet.detect(content))
                        if charset == 'ascii':
                            charset = 'unicode_escape'
                        if charset:
                            content = content.decode(charset)
                        else:
                            content = content.decode()
            if email_content_type == '':
                continue
        # if content == '':
        #     return "该邮件内容为空"

        return content
    except Exception as e:
        raise e


def getFrom(params):
    num = int(params['num'])
    popObj = params['obj']

    try:
        *_, mails, a = popObj.list()
        count = len(mails)
        if num > count:
            return "邮件序号错误"
        resp, lines, octets = popObj.retr(count - num + 1)
        msg_content = b'\r\n'.join(lines).decode('utf-8')

        msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象

        FROM = parseaddr(msg.get("From"))[0]

        if FROM:
            FROM = decode_str(FROM)

        return FROM
    except Exception as e:
        raise e


def getEmail(params):
    num = int(params['num'])
    popObj = params['obj']

    try:
        *_, mails, a = popObj.list()
        count = len(mails)
        if num > count:
            return "邮件序号错误"
        resp, lines, octets = popObj.retr(count - num + 1)
        msg_content = b'\r\n'.join(lines).decode('utf-8')
        msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
        email = parseaddr(msg.get("from"))[1]

        if email:
            email = decode_str(email)

        return email
    except Exception as e:
        raise e


def getDate(params):
    num = int(params['num'])
    popObj = params['obj']

    try:
        *_, mails, a = popObj.list()
        count = len(mails)
        if num > count:
            return "邮件序号错误"
        resp, lines, octets = popObj.retr(count - num + 1)
        msg_content = b'\r\n'.join(lines).decode('utf-8')
        msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
        date = msg.get("Date")

        return date
    except Exception as e:
        raise e


def getFile(params):
    num = int(params['num'])
    path = params['path']
    popObj = params['obj']

    try:
        resp, mails, octets = popObj.list()
        count = len(mails)
        if num > count:
            return "邮件序号错误"
        resp, lines, octets = popObj.retr(count - num + 1)
        msg_content = b'\r\n'.join(lines).decode('utf-8', errors='ignore')
        msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
        result = "该邮件没有附件"
        # downfile(msg, path)
        file_list = list()
        for part in msg.walk():
            file_name = part.get_filename()
            if file_name:
                fileName = Header(file_name)
                fileName = decode_header(fileName)  # 对附件名称进行解码
                filename = fileName[0][0]
                if fileName[0][1]:
                    filename = decode_str(str(filename, fileName[0][1]))  # 将附件名称可读化
                data = part.get_payload(decode=True)  # 下载附件
                if os.path.exists(os.path.join(path, filename)):
                    os.remove(os.path.join(path, filename))
                with open(os.path.join(path, filename), 'wb') as att_file:  # 在指定目录下创建文件，注意二进制文件需要用wb模式打开
                    att_file.write(data)  # 保存附件
                file_list.append(os.path.join(path, filename))
                result = "保存附件成功"
        print(result)
        return file_list
    except Exception as e:
        raise e


def decode_str(s):
    value, charset = decode_header(s)[0]
    if charset:
        print("header_charset:::", charset)
        value = value.decode(charset, errors='ignore')  # 如果文本中存在编码信息，则进行相应的解码
    return value


def guess_charset(msg):
    charset = msg.get_charset()  # 直接用get_charset()方法获取编码
    if charset is None:  # 如果获取不到，则在原始文本中寻找
        print("charset is none")
        content_type = msg.get('Content-Type', '').lower()
        pos = content_type.find('charset=')  # 找'charset='这个字符串
        if pos >= 0:  # 如果有，则获取该字符串后面的编码信息
            charset = content_type[pos + 8:].strip()
            print("find charset::", charset)
    return charset


def _format_addr(s):
    name, addr = parseaddr(s)
    return formataddr((Header(name, 'utf-8').encode(), addr))  # .encode('utf-8') if isinstance(addr, str) else addr))


def downfile(msg, path):
    for part in msg.walk():
        filename = part.get_filename()
        if filename is not None:  # 如果存在附件
            filename = decode_str(filename)  # 获取的文件是乱码名称，通过一开始定义的函数解码
            data = part.get_payload(decode=True)  # 取出文件正文内容
            # 此处可以自己定义文件保存位置
            f = open(os.path.join(path, filename), 'wb')
            f.write(data)
            f.close()


def get_mails_by_dates(params):
    try:
        # popObj = connectMail(params)
        popObj = params['obj']
        isFilter = False if params['filter'] == 'no' else True
        mails = list()
        # mails.clear()
        result = list()
        mails = popObj.list()[1]
        count = len(mails)
        print("邮件数量：", count)
        the_date = ''
        for index in range(1, count + 1):
            resp, lines, octets = popObj.retr(count - index + 1)
            msg_content = b'\r\n'.join(lines).decode('utf-8', errors='ignore')
            msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
            date = msg.get("Date")
            print('date>>>>>>>>>>>>>>>', date)
            if '(' in date:
                # date = date.replace('(CST)', '').strip()
                the_date = date[:date.index('(')].strip()
            elif 'GMT' in date and '(' not in date:
                the_date = date.replace('GMT', '').strip()
            else:
                the_date = date
            if "+" in the_date:
                the_date = the_date[:the_date.index('+')].strip()
            print('the-day>>>>>>>>>>>', the_date)
            # if params['server'] == '163' or params['server'] == '126':
            #     if ',' in the_date:
            #         the_date = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(the_date, "%a, %d %b %Y %H:%M:%S"))
            #     else:
            #         the_date = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(the_date, "%d %b %Y %H:%M:%S"))
            # else:
            if ',' in the_date:
                the_date = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(the_date, "%a, %d %b %Y %H:%M:%S"))
            else:
                the_date = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(the_date, "%d %b %Y %H:%M:%S"))

            if params['date_end'] >= the_date >= params['date_start']:
                mail_info = {'index': index}
                title = msg.get('Subject', '')
                if title:
                    title = decode_str(title)
                mail_info['title'] = title
                content = ''
                for part in msg.walk():
                    content_type = part.get_content_type()
                    # print(content_type)
                    charset = guess_charset(part)
                    # 如果有附件，则直接跳过
                    if part.get_filename() is not None:
                        continue
                    email_content_type = ''
                    if charset:
                        print("guess_charset::", charset)
                        if 'utf-8' in charset:
                            charset = 'utf-8'
                        if content_type == 'text/plain':
                            email_content_type = 'text'
                            content = part.get_payload(decode=True).decode(charset, errors='ignore')
                        elif content_type == 'text/html':
                            print("text/html===============")
                            email_content_type = 'html'
                            content = part.get_payload(decode=True).decode(charset, errors='ignore')
                            # print(content)
                            if isFilter:
                                bs = bs4.BeautifulSoup(content, "html.parser")
                                [b.extract() for b in bs(['script', 'style'])]
                                content = bs.get_text().lstrip().rstrip().encode(charset, errors='ignore')  # .decode()
                                charset = chardet.detect(content)['encoding']
                                print("编码：", chardet.detect(content))
                                if charset == 'ascii':
                                    charset = 'unicode_escape'
                                if charset:
                                    print('charset:>>>>>>>>>>>>>', charset)
                                    content = content.decode(charset)
                                else:
                                    content = content.decode(guess_charset(part))
                            # temp = '"%s"' % content
                            # content = json.loads(temp, strict=False).replace(u"\u3000", ' ')
                    if email_content_type == '':
                        continue
                mail_info['content'] = content
                FROM = parseaddr(msg.get("From"))[0]
                if FROM:
                    FROM = decode_str(FROM)
                mail_info['from'] = FROM
                email = parseaddr(msg.get("from"))[1]
                if email:
                    email = decode_str(email)
                mail_info['email'] = email
                mail_info['date'] = the_date

                mails.append(mail_info)

            if the_date < params['date_start']:
                break
        for mail in mails:
            if isinstance(mail, dict):
                result.append(mail)

        print('len>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', len(result))
        return result
    except Exception as e:
        raise e


if __name__ == '__main__':
    pass

