import json
from pprint import pprint
import pymysql
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import xlrd
import datetime
import os
import shutil
from traceback import print_exc


pymysql.install_as_MySQLdb()

"""
need:cx-Oracle==6.0, 32bit oracle11g-client(oci.dll, oraocci11.dll, oraociei11.dll), 32bit python.exe
"""
# 防止中文打印乱码
os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'


def database_operation(params):

    current_dir = os.path.split(os.path.realpath(__file__))[0]
    if os.path.exists(params['uiauto_config']['client_dir'] + '\\oci.dll') is False:
        shutil.copy(current_dir + '\\oci.dll', params['uiauto_config']['client_dir'] + '\\oci.dll')

    if os.path.exists(params['uiauto_config']['client_dir'] + '\\oraocci11.dll') is False:
        shutil.copy(current_dir + '\\oraocci11.dll', params['uiauto_config']['client_dir'] + '\\oraocci11.dll')

    if os.path.exists(params['uiauto_config']['client_dir'] + '\\oraociei11.dll') is False:
        shutil.copy(current_dir + '\\oraociei11.dll', params['uiauto_config']['client_dir'] + '\\oraociei11.dll')

    if params['database_type'] == 'oracle':
        connect_info = "oracle://{}:{}@{}/{}?utf8".format(params['username'], params['password'], params['host'],
                                                          params['database'])
    else:
        connect_info = "mysql://{}:{}@{}/{}?utf8".format(params['username'], params['password'], params['host'],
                                                          params['database'])
    engine = create_engine(connect_info, echo=False)
    db_session = sessionmaker(bind=engine)
    session = db_session()

    if params['data_type'] == 'sql':
        try:
            if params['sql'].lower().startswith('select'):

                res = session.execute(params['sql']).fetchall()
                res_list = []
                for one in res:
                    one_row = []
                    for clu in one:
                        if type(clu) is datetime.datetime:
                            clu = str(clu)
                        one_row.append(clu)
                    res_list.append(one_row)
                session.close()
                return res_list
            else:
                session.execute(params['sql'])
                session.commit()

                return True
        except Exception as ex:
            print_exc()
            return '执行出错：{}'.format(ex)
        finally:
            session.close()
    elif params['data_type'] == 'excel':

        excel_path = params['excel_path']
        table_name = params['table_name']
        column_map = eval(params['column_map'])
        book = xlrd.open_workbook(excel_path)
        sheel = book.sheet_by_index(0)
        nrows = sheel.nrows  # 总行数
        ncols = sheel.ncols
        table_head_map = {}

        # map_ = {'name': 'sql_name', 'age': 'sql_age', 'phone': 'sql_phone'}
        cols_str = ','.join([val for key, val in column_map.items()])

        for i in range(ncols):
            table_col_name = sheel.row_values(0)[i]
            table_head_map[table_col_name] = i
        try:
            for i in range(1, nrows):
                values = ','.join(["'%s'" % sheel.row_values(i)[table_head_map[j]] for j in column_map.keys()])
                sql = 'insert into %s (%s) values (%s)' % (table_name, cols_str, values)
                session.execute(sql)
                session.commit()
        except Exception as ex:
            print_exc()
            return '插入出错 %s' % ex
        finally:
            session.close()

    elif params['data_type'] == 'json':
        table_name = params['table_name']
        json_data = eval(str(params['json_data']))
        try:
            for d in json_data:
                field_name = ','.join([k for k in d.keys()])
                field_values = ','.join(["'%s'" % v for k, v in d.items()])
                sql = 'insert into %s (%s) values(%s)' % (table_name, field_name, field_values)
                session.execute(sql)
                session.commit()
        except Exception as ex:
            print_exc()
            return '插入出错 %s' % ex
        finally:
            session.close()
