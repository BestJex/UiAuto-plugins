import sys
import os
import cv2
from PIL import ImageGrab
import pyautogui
import aircv as ac
import numpy as np
import time


def cv_imread(filePath):
    cv_img = cv2.imdecode(np.fromfile(filePath, dtype=np.uint8), -1)
    return cv_img


# 截取当前全屏
def imbg_screenshot(imbg_path):
    px = ImageGrab.grab()
    width, high = px.size  # 获得当前屏幕的大小
    bbox = (0, 0, width, high)
    im = ImageGrab.grab(bbox)
    im.save(imbg_path)
    return imbg_path


# 获取匹配到的区域中心点坐标
def get_pos(imbg_path, imobj_path):
    # 含中文字符的路径会无法加载
    bg = cv_imread(imbg_path)  # 全屏截图
    obj = cv_imread(imobj_path)  # 目标区域
    pos = ac.find_template(bg, obj)  # 对比图片识别出坐标
    x = int(pos['result'][0])
    y = int(pos['result'][1])
    center_pos = (x, y)
    print(center_pos, '123123231')
    # print(center_pos)
    return center_pos


# 鼠标点击
def mouse(params):
    coordinate = params['pos']
    pyautogui.click(coordinate, button=params['button'], clicks=int(params['clicks']), duration=0.2)
    return "click done"


def main(params):
    # 运行时截全屏临时保存在插件目录下, 运行完即删除. 若选择保存此次临时截图, 则保存在用户自定义目录的smartclick_imlog文件夹下.
    imbg_path = os.path.join(sys.path[0], 'imbg_' + '_' + params['button'] + '_' + str(params['clicks']) + '_'
                             + time.strftime("%F_%H_%M_%S") + '.png')
    # 截全屏
    imbg_screenshot(imbg_path)
    params['pos'] = get_pos(imbg_path, params['imobj_path'])  # 识别返回的中心点坐标
    mouse(params)  # 进行点击
    # 判断是否需要保存全屏截图作为日志图片
    try:
        # 删除图片缓存
        os.remove(imbg_path)
        print("imbg_path has been deleted")
    except OSError:
        print("imbg_path is not exists, no need tohas been deleted")
    return
