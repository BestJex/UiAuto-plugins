from PIL import ImageGrab, Image
import re
import json
from selenium import webdriver
from selenium.webdriver.chrome import options
from selenium.common.exceptions import InvalidArgumentException
from browser import ResumeBrowser, generate_xpath
import time
import sys
from selenium.webdriver.common.action_chains import ActionChains
sys.path.insert(0, "C:\\UiAuto\\public\\pyscript\\base\\")


def goto_url(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        # driver = ResumeBrowser(
        #     browser_info['executor_url'], browser_info['session_id'])
        driver.get(params['url'])
        return None
    else:
        raise Exception('没有启动浏览器')


def click_ele(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        mouse_key = params['mouse_key']
        click_type = params['click_type']

        element = driver.find_element_by_xpath(params['xpath'])
        if element is not None:
            if mouse_key == 'left':
                if click_type == 'simple':
                    element.click()
                elif click_type == 'double':
                    ActionChains(driver).double_click(element).perform()
                else:
                    raise Exception(message="尚未支持该点击类型")
            elif mouse_key == 'right':
                if click_type == 'simple':
                    ActionChains(driver).context_click(element).perform()
                elif click_type == 'double':
                    ActionChains(driver).context_click(element).perform()
                    time.sleep(.1)
                    ActionChains(driver).context_click(element).perform()
                else:
                    raise Exception(message="尚未支持该点击类型")
            else:
                raise Exception(message="尚未支持该鼠标键的点击操作")
        else:
            raise Exception(message="找不到指定的元素")
    else:
        raise Exception(message="缺少参数：浏览器对象")


def openBrowser(params):
    try:
        executable_path = ""
        global driver
        if params['browser_type'] == "Internet Explorer":
            executable_path = params['uiauto_config']["client_dir"] + \
                "\\env\\webdriver\\win32\\IEDriverServer.exe"
            driver = webdriver.Ie(executable_path=executable_path)
        if params['browser_type'] == "Chrome":
            executable_path = params['uiauto_config']["client_dir"] + \
                "\\env\\webdriver\\win32\\chromedriver.exe"
            driver = webdriver.Chrome(executable_path=executable_path)

        driver.maximize_window()
        driver.get(params['url'])
        return driver
    except Exception as e:
        raise e


def getPage(params):
    # 重连浏览器
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        # browser_info = params['browser_info']
        driver = params['browser_info']
        return driver.page_source
    else:
        raise Exception('没有启动浏览器')


def injectJs(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        # driver = ResumeBrowser(
        #     browser_info['executor_url'], browser_info['session_id'])
        inject_result = driver.execute_script(params["js_code"])
        return inject_result
    else:
        raise Exception('没有启动浏览器')


def closeBrowser(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        # driver = ResumeBrowser(
        #     browser_info['executor_url'], browser_info['session_id'])
        driver.close()
        driver.quit()
        return None
    else:
        raise Exception('没有启动浏览器')


def getCookies(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        # driver = ResumeBrowser(
        #     browser_info['executor_url'], browser_info['session_id'])
        cookies = driver.get_cookies()
        return ';'.join([c['name'] + '=' + c['value'] for c in cookies])
    else:
        raise Exception('没有启动浏览器')


def remoteBrowser(params):
    try:
        if 'executor_url' in params.keys() and "session_id" in params.keys():
            driver = ResumeBrowser(
                params['executor_url'], params['session_id'])
            # executor_url = driver.command_executor._url
            # session_id = driver.session_id

            return driver
        else:
            raise Exception(message="缺少参数！")
    except Exception as e:
        raise e


def resolvingListData(params):
    try:
        result = {
            "headers": [],
            "items": []
        }
        if 'browser_info' in params.keys() and params['browser_info'] is not None \
                and 'css_selector' in params.keys() and params['css_selector'] is not None:
            driver = params["browser_info"]
            css_selector = params["css_selector"]
            # if 'executor_url' in browser_info.keys() and "session_id" in browser_info.keys():
            # driver = ResumeBrowser(
            #     browser_info['executor_url'], browser_info['session_id'])

            headers = []
            items = []
            element = driver.find_element_by_css_selector(css_selector)
            thead = element.find_element_by_tag_name("thead")

            ths = element.find_elements_by_tag_name("th")
            ignore_index = []
            column_index = []
            if ths is not None and len(ths) > 0:
                for i, th in enumerate(ths):
                    colspan = th.get_attribute("colspan")
                    if colspan:
                        start = column_index[len(
                            column_index) - 1] if len(column_index) > 0 else 0
                        for j in range(start + 1, start + int(colspan) + 1):
                            column_index.append(j)
                            ignore_index.append(
                                j) if th.text in params["ignore_column"] else None
                    else:
                        column_index.append(i)
                        ignore_index.append(i) if th.text in params["ignore_column"] else headers.append(
                            th.text.replace("↓", ""))
            result['headers'] = headers + params["add_column"]

            tbody = element.find_element_by_tag_name("tbody")
            trs = tbody.find_elements_by_tag_name("tr")
            if trs and len(trs) > 0:
                for tr in trs:
                    temp_item = []
                    tds = tr.find_elements_by_tag_name("td")
                    if tds and len(tds) > 0:
                        for i, td in enumerate(tds):
                            temp_item.append(
                                td.text) if i not in ignore_index else None
                        for ac in params["add_column"]:
                            temp_item.append("")
                        items.append(temp_item)
            result["items"] = items

            return result
        else:
            raise Exception(message="缺少参数！")
    except Exception as e:
        raise e


"""
TODO:
- 切换浏览器标签页
"""


def switchBrowserTag(params):
    try:
        target_handle = None
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'match_target' not in params.keys():
            raise Exception("缺少参数：匹配对象")
        elif 'match_content' not in params.keys():
            raise Exception("缺少参数：匹配内容")
        else:
            driver = params['browser_info']

            window_handles = driver.window_handles
            if len(window_handles) > 0:
                for handle in window_handles:
                    driver.switch_to_window(handle)
                    print("current_url:" + driver.current_url, driver.current_url == params['match_content'])
                    if target_handle is None and params['match_target'] == "Title" and driver.title == params['match_content']:
                        target_handle = handle
                    if target_handle is None and params['match_target'] == "Url" and driver.current_url == params['match_content']:
                        target_handle = handle
            else:
                raise Exception('浏览器没有标签页')

        if target_handle is None:
            raise Exception('没有找匹配的标签页')
        else:
            driver.switch_to_window(target_handle)
        return target_handle
    except Exception as e:
        raise e


def closeBrowserTag(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'tag_handle' not in params.keys():
            raise Exception("缺少参数：标签页句柄")
        else:
            driver = params['browser_info']

            window_handles = driver.window_handles
            if params['tag_handle'] not in window_handles:
                raise Exception('找不到与该句柄对应的标签页')
            else:
                driver.switch_to_window(params['tag_handle'])
                driver.close()
            return driver
    except Exception as e:
        raise e


def getBrowserRunStatus(params):
    status = False
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            # driver.get('http://www.baidu.com/')
            window_handles = driver.window_handles
            if len(window_handles) > 0:
                status = True
            else:
                status = False
    except Exception:
        status = False

    return status


def browserForward(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.forward()
    except Exception as e:
        raise e


def browserBack(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.back()
    except Exception as e:
        raise e


def browserRefresh(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.refresh()
    except Exception as e:
        raise e


def stopLoadPage(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.set_page_load_timeout(0)
    except Exception as e:
        raise e


def waitForPageLoaded(params):
    try:
        result = False
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            if params['element_type'] == 'UI':
                if 'html' not in params['element_info'].keys():
                    raise Exception('检测元素为非网页元素')
                html = params['element_info']['html']
                xpath = generate_xpath(html=html)
                while True:
                    try:
                        driver.find_element_by_xpath(xpath)
                        result = True
                        break
                    except Exception as e:
                        print(e)
            elif params['element_type'] == 'Xpath':
                while True:
                    try:
                        driver.find_element_by_xpath(params['element_xpath'])
                        result = True
                        break
                    except Exception as e:
                        print(e)
            else:
                raise Exception("暂不支持当前元素类型")
        return result
    except Exception as e:
        raise e


def downloadFile(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'download_url' not in params.keys() or params['download_url'] is None:
            raise Exception('缺少参数：下载链接')
        elif 'save_path' not in params.keys() or params['save_path'] is None:
            raise Exception('缺少参数：保存路径')
        elif 'is_async_download' not in params.keys() or params['is_async_download'] is None:
            raise Exception('缺少参数：是否同步下载')
        else:
            pass
    except Exception as e:
        raise e


def getPageUrl(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            return driver.current_url
    except Exception as e:
        raise e


def getPageTitle(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            return driver.title
    except Exception as e:
        raise e


def setPageCookies(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'cookies' not in params.keys() or params['cookies'] is None:
            raise Exception("缺少参数：Cookies数据")
        else:
            driver = params['browser_info']
            driver.add_cookie(params['cookies'])
    except Exception as e:
        raise e


def browserCapture(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'capture_type' not in params.keys() or params['capture_type'] is None:
            raise Exception("缺少参数：截图方式")
        elif 'save_path' not in params.keys() or params['save_path'] is None:
            raise Exception("缺少参数：保存路径")
        elif 'file_name' not in params.keys() or params['file_name'] is None:
            raise Exception("缺少参数：保存文件名")
        elif params['capture_type'] == 'Element' and ('target_element' not in params.keys() or params['target_element'] is None):
            raise Exception("缺少参数：网页元素")
        else:
            driver = params['browser_info']
            img_file_path = '%s\\%s.png' % (
                params['save_path'], params['file_name'])
            if params['capture_type'] == 'Window':
                driver.get_screenshot_as_file(img_file_path)
            elif params['capture_type'] == 'Area':
                full_image_path = '%s\\full_image.png' % params['save_path']
                driver.get_screenshot_as_file(full_image_path)
                img = Image.open(full_image_path)
                cropped = img.crop((int(params['x']), int(params['y']), int(params['x']) + int(params['width']), int(params['y']) + int(params['height'])))
                cropped.save(img_file_path)
            elif params['capture_type'] == 'Element':
                html = params['target_element']['html']
                xpath = generate_xpath(html=html)
                el = driver.find_element_by_xpath(xpath)
                el.screenshot(img_file_path)

                # im = ImageGrab.grab(bbox=(location['x'], \
                #     location['y'], \
                #     location['x'] + rect['width'], \
                #     location['y'] + rect['height']))

                # im.save(img_file_path)
            else:
                raise Exception('该截图方式尚未支持')
            return img_file_path
    except Exception as e:
        raise e


def getScrollPosition(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            scroll_pos = driver.execute_script("""
                return {
                    scrollTop: document.documentElement.scrollTop,
                    scrollLeft: document.documentElement.scrollLeft
                }
            """)

            return scroll_pos
    except Exception as e:
        raise e


def setScrollPosition(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'scroll_type' not in params.keys() or params['scroll_type'] is None:
            raise Exception("缺少参数：滚动方向")
        elif 'scroll_position' not in params.keys() or params['scroll_position'] is None:
            raise Exception("缺少参数：滚动位置")
        else:
            driver = params['browser_info']

            if params['scroll_type'] == 'Horizontal':
                driver.execute_script(
                    'window.scrollBy(%s - document.documentElement.scrollLeft, 0)' % params['scroll_position'])
            elif params['scroll_type'] == 'Vertical':
                driver.execute_script(
                    'window.scrollBy(0, %s - document.documentElement.scrollTop)' % params['scroll_position'])
            else:
                pass

            return None
    except Exception as e:
        raise e


def switchToIframe(params):
    try:
        if 'switch_type' not in params.keys():
            if 'browser_info' not in params.keys() or params['browser_info'] is None:
                raise Exception("缺少参数：浏览器对象")
            elif 'element_no' not in params.keys() or params['element_no'] is None:
                raise Exception("缺少参数：元素序号")
            else:
                driver = params['browser_info']

                iframes = driver.find_elements_by_tag_name('iframe')
                frames = driver.find_elements_by_tag_name('frame')
                if len(iframes) > 0 or len(frames) > 0:
                    driver.switch_to.frame(int(params['element_no']) - 1)
                else:
                    raise Exception('当前页面不存在iframe元素')
        else:
            if 'browser_info' not in params.keys() or params['browser_info'] is None:
                raise Exception("缺少参数：浏览器对象")
            else:
                driver = params['browser_info']

                if params['switch_type'] == 'Index':
                    iframes = driver.find_elements_by_tag_name('iframe')
                    frames = driver.find_elements_by_tag_name('frame')
                    if len(iframes) > 0 or len(frames) > 0:
                        driver.switch_to.frame(int(params['element_no']) - 1)
                    else:
                        raise Exception('当前页面不存在iframe元素')
                elif params['switch_type'] == 'Xpath':
                    element = driver.find_element_by_xpath(params['element_xpath'])
                    if element is not None:
                        driver.switch_to.frame(element)
    except Exception as e:
        raise e


def switchDefaultContent(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.switch_to.default_content()
    except Exception as e:
        raise e


def switchToWindow(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'window_no' not in params.keys() or params['window_no'] is None:
            raise Exception("缺少参数：元素序号")
        else:
            driver = params['browser_info']
            driver.switch_to_window(driver.window_handles[int(params['window_no']) - 1])
            return driver
    except Exception as e:
        raise e


def selectClick(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'xpath' not in params.keys() or params['xpath'] is None:
            raise Exception("缺少参数：元素XPATH")
        elif 'selectedText' not in params.keys() or params['selectedText'] is None:
            raise Exception("缺少参数：选择项")
        else:
            driver = params['browser_info']
            driver.find_element_by_xpath(params['xpath']).click()
            time.sleep(.2)
            driver.find_element_by_xpath("//option[text()='" + params['selectedText'] + "']").click()
            return None
    except Exception as e:
        raise e


if __name__ == "__main__":
    # switchBrowserTag({
    #     'browser_info': {
    #         'executor_url': 'http://127.0.0.1:51512',
    #         'session_id': '44f7e19d35aec4c9d442117af1b14f91'
    #     },
    #     'match_target': "Title",
    #     'match_content': "设置"
    # })

    driver = openBrowser({
        'browser_type': 'Internet Explorer',
        'uiauto_config': {
            "client_dir": 'Z:\\workspace\\legion\\code\\UiAuto\\client'
        },
        'url': 'http://146.4.80.158:81/zxxt/login.jsp'
    })

    # switchToIframe({
    #     'browser_info': driver,
    #     'element_no': '1'
    # })

    # driver.find_element_by_xpath('//input[@id="kw"]').send_keys('Python')
    print(dir(driver))
    print(driver.current_url)
    # time.sleep(10)
    driver.quit()
