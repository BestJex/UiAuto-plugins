import subprocess
import sys


def external_python_executor(params=None):
    p = subprocess.Popen([params['python_path'], params['main_file']], stdout=subprocess.PIPE, cwd=params['directory'])
    data = p.communicate()  #如果启用此相会阻塞主程序
    p.wait()              #等待子程序运行完毕
    print(data)
    return data


if __name__ == "__main__":
    print(sys.argv)
    # external_python_executor({
    #     "python_path": "C:\\UiAuto\\env\\python\\win32\\python.exe",
    #     "main_file": "test.py",
    #     "directory": "Z:\\Users\\dreamboyfire\\workspace\\legion\\code\\gitee\\UiAuto-plugins\\external_python_executor"
    # })
