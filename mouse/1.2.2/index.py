# import pyautogui
import time
from pymouse import PyMouse
import sys
import json
import base64
import uiautomation as uiauto
from browser import ResumeBrowser, generate_xpath
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from win32 import win32api, win32gui, win32print
from win32.lib import win32con
from win32.win32api import GetSystemMetrics

mouse = PyMouse()


def click(params):
    # pyautogui.moveTo(params['x_coordinate'], params['y_coordinate'])
    mouse.move(int(params['x_coordinate']), int(params['y_coordinate']))
    time.sleep(.2)
    # pyautogui.click(params['x_coordinate'], params['y_coordinate'])
    mouse.click(int(params['x_coordinate']), int(params['y_coordinate']))
    return


def move_to(params):
    mouse.move(int(params['x_coordinate']), int(params['y_coordinate']))
    return


def switch_frame(driver, iframe):
    try:
        iframe_el = driver.find_element_by_xpath(iframe['full_xpath'])
        if iframe_el is not None:
            driver.switch_to.frame(iframe_el)
            if iframe['next_frame'] is not None:
                swich_frame(driver, iframe['next_frame'])
    except Exception as e:
        driver.switch_to.default_content()


def mouse_click(params):
    try:
        element_type = params['element_type']
        mouse_key = params['mouse_key']
        click_type = params['click_type']
        # target_element = params['target_element']

        element = None
        if params['element_type'] == 'Xpath':
            if 'browser_info' in params.keys() and params['browser_info'] is not None:
                driver = params['browser_info']
                element = driver.find_element_by_xpath(params['element_xpath'])
                driver.execute_script("arguments[0].scrollIntoView()", element)
                if element is not None:
                    if mouse_key == 'left':
                        if click_type == 'simple':
                            element.click()
                        elif click_type == 'double':
                            ActionChains(driver).double_click(element).perform()
                        else:
                            raise Exception("尚未支持该点击类型")
                    elif mouse_key == 'right':
                        if click_type == 'simple':
                            ActionChains(driver).context_click(element).perform()
                        elif click_type == 'double':
                            ActionChains(driver).context_click(element).perform()
                            time.sleep(.1)
                            ActionChains(driver).context_click(element).perform()
                        else:
                            raise Exception("尚未支持该点击类型")
                    else:
                        raise Exception("尚未支持该鼠标键的点击操作")
            else:
                raise Exception("缺少参数：浏览器对象")
        elif params['element_type'] == 'Browser':
            if params['browser_info'] is None or params['browser_info'] == '':
                raise Exception("缺少参数：浏览器对象")
            if params['target_browser_element'] is None or params['target_browser_element'] == '':
                raise Exception("缺少参数：选择目标")

            

            driver = params['browser_info']
            driver.switch_to.default_content()

            target_element = params['target_browser_element']
            html = target_element['html']

            if html['frame'] is not None:
                switch_frame(driver, html['frame'])

            try:
                element = driver.find_element_by_xpath(html['xpath'])
            except NoSuchElementException:
                element = (driver.find_element_by_xpath(html['full_xpath']) if 'full_xpath' in html.keys() else None)

            driver.execute_script("arguments[0].scrollIntoView()", element)

            if element is not None:
                if mouse_key == 'left':
                    if click_type == 'simple':
                        element.click()
                    elif click_type == 'double':
                        ActionChains(driver).double_click(element).perform()
                    else:
                        raise Exception("尚未支持该鼠标操作类型")
                elif mouse_key == 'right':
                    if click_type == 'simple':
                        ActionChains(driver).context_click(element).perform()
                    elif click_type == 'double':
                        ActionChains(driver).context_click(element).perform()
                        time.sleep(.1)
                        ActionChains(driver).context_click(element).perform()
                    else:
                        raise Exception("尚未支持该鼠标操作类型")
                else:
                    raise Exception("尚未支持该鼠标键的操作")
            else:
                raise Exception("无法定位元素")

        elif params['element_type'] == 'Native':
            if params['target_native_element'] is None or params['target_native_element'] == '' or 'wnd' not in params['target_native_element'].keys():
                raise Exception("缺少参数：选择目标")

            wnd = params['target_native_element']['wnd']
            if "control_type_name" not in wnd.keys() or wnd["control_type_name"] is None or wnd["control_type_name"] == "":
                raise Exception("客户端界面元素没有control_type_name")
            elif "name" not in wnd.keys() or wnd["name"] is None or wnd["name"] == "":
                raise Exception("客户端界面元素没有name属性")
            elif hasattr(uiauto, wnd["control_type_name"]) is False:
                raise Exception("uiautomation不支持目标元素类型")
            else:
                control = getattr(uiauto, wnd["control_type_name"])(
                    Name=wnd['name'])
                if mouse_key == 'left':
                    if click_type == 'simple':
                        if hasattr(control, "Click"):
                            control.Click()
                        else:
                            raise Exception("当前界面元素不支持键盘输入")
                    elif click_type == 'double':
                        if hasattr(control, "DoubleClick"):
                            control.DoubleClick()
                        else:
                            raise Exception("当前界面元素不支持键盘输入")
                    else:
                        raise Exception("尚未支持该点击类型")
                elif mouse_key == 'right':
                    if click_type == 'simple':
                        if hasattr(control, "RightClick"):
                            control.RightClick()
                        else:
                            raise Exception("当前界面元素不支持键盘输入")
                    elif click_type == 'double':
                        if hasattr(control, "RightClick"):
                            control.RightClick()
                            control.RightClick()
                        else:
                            raise Exception("当前界面元素不支持键盘输入")
                    else:
                        raise Exception("尚未支持该点击类型")
                else:
                    raise Exception("尚未支持该鼠标键的点击操作")
        else:
            raise Exception("尚未支持当前元素的操作")

        return None
    except Exception as e:
        raise e


def get_real_resolution():
    """获取真实的分辨率"""
    hDC = win32gui.GetDC(0)
    # 横向分辨率
    w = win32print.GetDeviceCaps(hDC, win32con.DESKTOPHORZRES)
    # 纵向分辨率
    h = win32print.GetDeviceCaps(hDC, win32con.DESKTOPVERTRES)
    return w, h


def get_screen_size():
    """获取缩放后的分辨率"""
    w = GetSystemMetrics (0)
    h = GetSystemMetrics (1)
    return w, h