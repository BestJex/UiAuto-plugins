const fs = require("fs");
const _ = require('lodash');

exports.getLatestFilePath = function (params) {
    
    return new Promise((resolve, reject) => {
        var fileNames = fs.readdirSync(params.path);
        let result_file = {};

        _.each(fileNames, fileName => {
            if (fileName === '.DS_Store') return;
            let filePath = params.path + '/' + fileName;

            var stat = fs.statSync(filePath);
            
            if (!result_file.path || result_file.stat.ctimeMs < stat.ctimeMs) {
                result_file.stat = stat;
                result_file.path = filePath;
            }
        })

        resolve(result_file.path);
    })
}