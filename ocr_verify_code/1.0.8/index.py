import uuid
import os
import sys
import json
from selenium.webdriver import Remote
from selenium.webdriver.chrome import options
from selenium.common.exceptions import InvalidArgumentException
from PIL import Image
from requests.sessions import Session


class ReuseBrowser(Remote):
    def __init__(self, command_executor, session_id):
        self.r_session_id = session_id
        self.w3c = True
        Remote.__init__(self, command_executor=command_executor, desired_capabilities={})

    def start_session(self, capabilities, browser_profile=None):
        """
        重写start_session方法
        """
        if not isinstance(capabilities, dict):
            raise InvalidArgumentException("Capabilities must be a dictionary")
        if browser_profile:
            if "moz:firefoxOptions" in capabilities:
                capabilities["moz:firefoxOptions"]["profile"] = browser_profile.encoded
            else:
                capabilities.update({'firefox_profile': browser_profile.encoded})
        self.capabilities = options.Options().to_capabilities()
        self.session_id = self.r_session_id


def ocr_verify_code_(file_path, yzm_type='', try_times=5, api_url='', username='', password=''):
    """
    验证码识别
    :param file_path: 待识别的图片路径（绝对路径）
    :param yzm_type：验证码类型（默认为空）
                    具体类型可参考：https://www.jsdati.com/docs/price
    :param api_url: API接口(默认为空)
    :param username: 账户(默认为空)
    :param password: 密码(默认为空)
    :return:
    """

    try:
        # 要上传到打码平台的数据
        # api_post_url = "http://v1-http-api.jsdama.com/api.php?mod=php&act=upload"
        api_post_url = api_url
        yzm_min = ''
        yzm_max = ''
        tools_token = ''
        data = {"user_name": username,
                "user_pw": password,
                "yzm_minlen": yzm_min,
                "yzm_maxlen": yzm_max,
                "yzmtype_mark": yzm_type,
                "zztool_token": tools_token,
                }
        files = {
            'upload': (os.path.basename(file_path), open(file_path, 'rb'), 'image/png')
        }
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',
            'Connection': 'keep-alive',
            'Host': 'v1-http-api.jsdama.com',
            'Upgrade-Insecure-Requests': '1'
        }
        # 实例化连接对象
        s = Session()
        result = s.post(api_post_url, headers=headers, data=data, files=files, verify=False, timeout=30)
        # 返回数据
        result = result.text
        res = json.loads(result)
        if res['result']:
            return res['data']['val']
        else:
            return None
    except Exception as err:
        if try_times > 0:
            return ocr_verify_code(file_path, yzm_type, try_times - 1)
        else:
            return 'OCR Error: {}'.format(err)


def console_result(verify_code):
    sys.stdout.write('<uiauto:result>{}</uiauto:result>'.format(verify_code))


def ocr_verify_code(params):
    api_url = params['api_url']
    username = params['username']
    password = params['password']
    browser_info = params['browser_info']
    css_selector = params['css_selector']
    yzm_type = params.get('yzm_type', '')
    # 重连浏览器
    # driver = ReuseBrowser(browser_info['executor_url'], browser_info['session_id'])
    driver = params['browser_info']
    image_path = '{}.png'.format(uuid.uuid1())
    father_path = os.path.dirname(__file__)
    image_path = os.path.join(father_path, image_path)
    driver.save_screenshot(image_path)
    verify_ele = driver.find_element_by_css_selector(css_selector)
    im = Image.open(image_path)
    location = verify_ele.location
    size = verify_ele.size
    left = location['x']
    top = location['y']
    right = left + size['width']
    bottom = top + size['height']
    im = im.crop((left, top, right, bottom))
    im.save(image_path)
    verify_code = ocr_verify_code_(file_path=image_path, yzm_type=yzm_type, api_url=api_url, username=username, password=password)
    try:
        os.remove(image_path)
        # print(image_path)
    except PermissionError:
        pass
    return verify_code
