import os
from PIL import ImageGrab
import sys
import time


def Screenshot(params):
    if params['path']:
        try:
            path = params['path'] + '\\UIAuto_photo\\' + time.strftime("%F")
            if os.path.exists(path) is False:
                os.makedirs(path)
            save_path = path + '\\UIAuto_' + time.strftime("%F_%H_%M_%S") + '.png'
        except:
            return 'wrong path'
    else:
        return 'There is no path to save'
    try:
        px = ImageGrab.grab()
        width, high = px.size  # 获得当前屏幕的大小
        bbox = (0, 0, width, high)
        im = ImageGrab.grab(bbox)
        im.save(save_path)
        return save_path
    except Exception as e:
        return 'Screenshot failed to save\n{}'.format(e)


def main(params):
    try:
        verify_code = Screenshot(params)
    except:
        verify_code = 'Screenshot() ERROR'
    console_result(verify_code)
    return verify_code


def console_result(verify_code):
    sys.stdout.write('<uiauto:result>{}</uiauto:result>'.format(verify_code))
