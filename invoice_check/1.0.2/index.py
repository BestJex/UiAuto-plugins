import time
import os
import json

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver import ChromeOptions
from PIL import Image
from requests.sessions import Session

option = ChromeOptions()
# option.add_experimental_option('excludeSwitches', ['enable-automation'])
# 这个是一个用来控制chrome以无界面模式打开的浏览器
# 创建一个参数对象，用来控制chrome以无界面的方式打开
# 后面的两个是固定写法 必须这么写
option.add_argument('--headless')
option.add_argument('--disable-gpu')
option.add_argument('--ignore-certificate-errors')


def ocr_verify_code(file_path, yzm_type='', try_times=5, username='', password=''):
    """
    验证码识别
    :param file_path: 待识别的图片路径（绝对路径）
    :param yzm_type：验证码类型（默认为空）
                    具体类型可参考：https://www.jsdati.com/docs/price
    :param api_url: API接口(默认为空)
    :param username: 账户(默认为空)
    :param password: 密码(默认为空)
    :return:
    """

    try:
        # 要上传到打码平台的数据
        api_post_url = "http://v1-http-api.jsdama.com/api.php?mod=php&act=upload"
        # api_post_url = api_url
        yzm_min = ''
        yzm_max = ''
        tools_token = ''
        data = {"user_name": username,
                "user_pw": password,
                "yzm_minlen": yzm_min,
                "yzm_maxlen": yzm_max,
                "yzmtype_mark": yzm_type,
                "zztool_token": tools_token,
                }
        files = {
            'upload': (os.path.basename(file_path), open(file_path, 'rb'), 'image/png')
        }
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',
            'Connection': 'keep-alive',
            'Host': 'v1-http-api.jsdama.com',
            'Upgrade-Insecure-Requests': '1'
        }
        # 实例化连接对象
        s = Session()
        result = s.post(api_post_url, headers=headers, data=data, files=files, verify=False, timeout=30)
        # 返回数据
        result = result.text
        res = json.loads(result)
        if res['result']:
            return res['data']['val']
        else:
            return None
    except Exception as err:
        if try_times > 0:
            return ocr_verify_code(file_path, yzm_type, try_times - 1, username=username, password=password)
        else:
            return 'OCR Error: {}'.format(err)


def invoice_check(params):
    fpdm = params['fpdm']
    fphm = params['fphm']
    kprq = params['kprq']
    kjje = params['kjje']
    yzm_user = params['yzm_user']
    yzm_pwd = params['yzm_pwd']

    s = time.time()
    driver_path = os.path.join(os.path.split(os.path.realpath(__file__))[0], 'chromedriver_v83.exe')
    driver = webdriver.Chrome(
        executable_path=driver_path, chrome_options=option)
    # driver.maximize_window()
    driver.set_window_size(1280, 1024)  # 分辨率 1280*800
    # 防止反爬检测
    driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
        "source": """
        Object.defineProperty(navigator, 'webdriver', {
          get: () => undefined
        })
      """
    })

    driver.implicitly_wait(30)
    driver.get('https://inv-veri.chinatax.gov.cn/index.html')
    try:
        if 'details-button' in driver.page_source:
            # if 1:
            driver.find_element_by_id('details-button').click()
            driver.find_element_by_id('proceed-link').click()

        driver.find_element_by_id('fpdm').send_keys(fpdm)
        driver.find_element_by_id('fphm').send_keys(fphm)
        driver.execute_script(f'document.getElementById("kprq").value="{kprq}"')
        driver.find_element_by_id('kjje').send_keys(kjje)
        flag = 0
        while True:
            if flag > 0:
                driver.find_element_by_id('yzm').clear()
                driver.find_element_by_id('yzm_img').click()
            time.sleep(2)
            flag += 1
            driver.save_screenshot('screen.png')
            img = Image.open('screen.png')
            verify_info_text = driver.find_element_by_id('yzminfo')
            verify_info = driver.find_element_by_id('yzm_img')
            x1, y1 = verify_info.location['x'], verify_info_text.location['y']
            x2, y2 = verify_info_text.location['x'] + verify_info_text.size['width'], verify_info.location['y'] + \
                     verify_info.size['height']
            img = img.crop((x1, y1, x2, y2))
            img.save('yzm.png')
            s1 = time.time()
            yzm = ocr_verify_code('yzm.png', yzm_type='1103', username=yzm_user, password=yzm_pwd)
            print('验证码识别耗时：%.2f 秒' % (time.time() - s1))
            if not yzm:
                print('验证码识别失败开始重试')
                continue
            print(f'验证码识别结果：{yzm}')
            driver.find_element_by_id('yzm').send_keys(yzm)
            driver.execute_script("document.getElementById('checkfp').click()")
            time.sleep(1)
            if '验证码错误' in driver.page_source:
                print('验证码错误,')
                driver.find_element_by_id('popup_ok').click()
                continue
            break
        print('本次校验共耗时 %.2f 秒' % (time.time() - s))
        if '超过' in driver.page_source:
            print('****查询成功但超过查询次数')
            text = json.dumps({'校验状态': True, '真伪': True, '结果信息': '查询成功但超过本日查询次数：5'}, ensure_ascii=False)
        else:
            driver.switch_to.frame('dialog-body')
            for i in range(20):
                if '次数' not in driver.page_source:
                    time.sleep(.2)
                    continue
                break
            if 'sbbh_dzfp' in driver.page_source:
                soup = BeautifulSoup(driver.page_source, 'html.parser')
                goods_trs = soup.find('table', class_='fppy_table_box').find('tbody').find_all('tr')[1:-2]
                good_data = []
                for tr in goods_trs:
                    tds = tr.find_all('td')
                    good_data.append({
                        '货物或应税劳务服务名称': tds[0].text,
                        '规格型号': tds[1].text,
                        '单位': tds[2].text,
                        '数量': tds[3].text,
                        '单价': tds[4].text,
                        '金额': tds[5].text,
                        '税率': tds[6].text,
                        '税额': tds[7].text,
                    })
                data = {
                    '校验状态': True,
                    '真伪': True,
                    '查验次数': soup.find(id='cycs').text.split('：')[1],
                    '查验时间': soup.find(id='cysj').text.split('：')[1],
                    '标题': soup.find(id='fpcc_dzfp').text,
                    '发票代码': soup.find(id='fpdm_dzfp').text,
                    '发票号码': soup.find(id='fphm_dzfp').text,
                    '开票日期': soup.find(id='kprq_dzfp').text,
                    '校验码': soup.find(id='jym_dzfp').text,
                    '机器编号': soup.find(id='sbbh_dzfp').text,
                    '购买方名称': soup.find(id='gfmc_dzfp').text,
                    '购买方纳税人识别号': soup.find(id='gfsbh_dzfp').text,
                    '购买方地址电话': soup.find(id='gfdzdh_dzfp').text,
                    '购买方开户行及账号': soup.find(id='gfyhzh_dzfp').text,
                    '商品信息总': good_data,
                    '价税合计大写': soup.find(id='jshjdx_dzfp').text,
                    '价税合计小写': soup.find(id='jshjxx_dzfp').text,
                    '销售方名称': soup.find(id='xfmc_dzfp').text,
                    '销售方纳税人识别号': soup.find(id='xfsbh_dzfp').text,
                    '销售方地址电话': soup.find(id='xfdzdh_dzfp').text,
                    '销售方开户行及账号': soup.find(id='xfyhzh_dzfp').text,
                    '备注': soup.find(id='bz_dzfp').text,
                }
                text = json.dumps(data, ensure_ascii=False)
            elif '结果' in driver.page_source:
                text = json.dumps({'校验状态': True, '真伪': False, '结果信息': driver.find_element_by_id('cyjg').text},
                                  ensure_ascii=False)
            else:
                text = json.dumps({'校验状态': False, '真伪': False, '结果信息': '校验出错'}, ensure_ascii=False)
        return text
    except Exception as ex:
        return json.dumps({'校验状态': False, '真伪': False, '结果信息': '校验失败：%s' % ex}, ensure_ascii=False)
    finally:
        driver.quit()
