const _ = require("lodash");
const axios = require('axios');
const qs = require('qs');

exports.httpRequestFn = function (params) {
    console.warn("httpRequestFn")
    console.log(params.httpRequestParams)
    // params.httpRequestParams && (params.httpRequestParams = JSON.parse(params.httpRequestParams));

    console.log(params.httpRequestParams)
    console.warn(params)
    const promise = new Promise((resolve, reject) => {
        if (params.httpRequestMethod == 'post') {
            post(params).then(post_result => {
                let result;
                console.log('------------------------------')
                console.log(params);
                console.log(post_result);
                switch (params.requireContent) {
                    case 'data':
                        result = post_result.data
                        break;

                    case 'cookie':
                        console.log(post_result.headers['set-cookie'])
                        result = post_result.headers['set-cookie'][0].split(';')[0];
                        break;

                    default:
                        break;
                }
                console.warn("httpRequest----post_result")
                console.warn(result);
                resolve(result);
            }).catch(err => {
                console.warn("httpRequest----post----err")
                console.warn(err.stack);
                resolve({
                    isSuccess: false,
                    code: "0x0001",
                    errorMsg: err.stack,
                    data: null,
                    params: params
                })
            })
        } else if (params.httpRequestMethod == 'get') {
            get(params).then(data => {
                console.warn("httpRequest---get-----data")
                console.warn(data.data)
                resolve(data.data);
            }).catch(err => {
                console.warn("httpRequest--------errget")
                console.warn(err)
                resolve({
                    isSuccess: false,
                    code: "0x0001",
                    errorMsg: err.stack,
                    data: null,
                    params: params
                })
            })
        } else {
            console.warn(params)
            reject('httpRequestMethod error');
        }
    });

    return promise;
}

// 创建axios实例
var instance = axios.create({
    timeout: 1000 * 12
});

instance.interceptors.request.use(
    error => Promise.error(error))

var defaultHeader = {
    'Content-Type': 'application/x-www-form-urlencoded'
};
// 响应拦截器
instance.interceptors.response.use(
    // 请求成功
    res => res.status === 200 ? Promise.resolve(res) : Promise.reject(res),
    // 请求失败
    error => {
        const {
            response
        } = error;
        if (response) {
            // 请求已发出，但是不在2xx的范围
            errorHandle(response.status, response.data.message);
            return Promise.reject(response);
        }
    });

/**
 * 请求失败后的错误统一处理
 * @param {Number} status 请求失败的状态码
 */
const errorHandle = (status, other) => {
    // 状态码判断
    switch (status) {
        // 401: 未登录状态
        case 401:
            console.log('401')
            break;
        // 403 token过期
        case 403:
            console.log('403')
            localStorage.removeItem('token');
            break;
        // 404请求不存在
        case 404:
            console.log('404')
            break;
        default:
            console.log(other);
    }
}

const get = (params) => {
    console.log("instance.get ---params");
    console.log(params);
    let param = params.passingMethod == 'form-data' ? qs.stringify(params.httpRequestParams) : params.httpRequestParams
    return new Promise((resolve, reject) => {
        axios.get(params.httpRequestLink, {
            headers: params.httpRequestHeader || defaultHeader,
            params: param
        })
            // instance.get(params.httpRequestLink, {
            //     headers: params.httpRequestHeader || defaultHeader,
            // })
            .then(res => {
                console.log("instance.get ---res");
                console.log(res.data);
                resolve(res);
            })
            .catch(err => {
                console.log("instance.get ---err");
                console.log(err);
                reject(err)
            })
    })
}
const post = (params) => {
    return new Promise((resolve, reject) => {
        console.log('-------params.httpRequestParams----------');
        console.log(params)
        let param = params.passingMethod == 'form-data' ? qs.stringify(params.httpRequestParams) : params.httpRequestParams
        axios.post(params.httpRequestLink, param, {
            headers: params.httpRequestHeader || defaultHeader
        })
            .then(res => {
                console.log(res.data)
                resolve(res);
            })
            .catch(err => {
                console.log(err)
                reject(err)
            })
    })
}
