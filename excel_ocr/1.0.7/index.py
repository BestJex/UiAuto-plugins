import os
import base64
import datetime
import requests
import json
import urllib.request as urllib2
import urllib


def encode_image(path):
    # 大于4M的图片无效
    try:
        size = os.path.getsize(path)
        if size > 4194304:
            return "image size too big"
        f = open(path, 'rb')
        # 进行图像base64编码
        img = base64.b64encode(f.read())
        return img
    except Exception as error:
        return error


def input_photo(params):
    url = params['url']
    request_params = params['request_params']
    # 对请求参数进行编码传入urllib2.Request()
    encode_params = urllib.parse.urlencode(request_params).encode("utf-8")
    request = urllib2.Request(url, encode_params)
    request.add_header('Content-Type', 'application/x-www-form-urlencoded')
    response = urllib2.urlopen(request)
    content = response.read()
    return str(content.decode("utf-8"))


def excel_ocr(params):
    # https://ai.baidu.com/docs#/Auth/75d80ed1
    # https://console.bce.baidu.com/ai/?_=1566193744774&fromai=1#/ai/ocr/app/detail~appId=289184
    # token示例： 24.ab1d7952f58aaed2a3cde0028e973037.2592000.1566550381.282335-16875827
    # 百度ocr技术提供相关api支持
    api_key = params['api_key']
    secret_ket = params['secret_ket']
    token_url = f'https://aip.baidubce.com/oauth/2.0/token?' \
        f'grant_type=client_credentials&client_id={api_key}&client_secret={secret_ket}&'
    res = requests.post(token_url)
    if 'access_token' in res.json().keys():
        access_token = res.json()['access_token']
        dict_ = dict()
        dict_['path'] = params['img_path']
        dict_['url'] = 'https://aip.baidubce.com/rest/2.0/solution/v1/form_ocr/request?access_token=' + access_token
        # 对图片进行base64编码
        dict_['request_params'] = {'image': encode_image(dict_['path'])}
        dict_['request_params']['is_sync'] = 'true'
        dict_['request_params']['request_type'] = params['type']

        if params['type'] == 'excel':
            result = json.loads(input_photo(dict_))
            file_url = result['result']['result_data']
            request = urllib2.Request(file_url)
            request.add_header('Content-Type', 'application/x-www-form-urlencoded')
            response = urllib2.urlopen(request)
            content = response.read()
            save_path = os.path.join(params['save_path'], datetime.datetime.now().strftime('%Y-%m-%d %H%M%S') + '.xls')
            f = open(save_path, 'wb')
            f.write(content)
            f.close()
        elif params['type'] == 'json':
            return json.dumps(input_photo(dict_), ensure_ascii=False)
    elif 'unknown client id' in res.text:
        return 'error: API Key不正确'
    elif 'Client authentication failed' in res.text:
        return 'error: Secret Key不正确'
    else:
        return 'error: 未知错误'
