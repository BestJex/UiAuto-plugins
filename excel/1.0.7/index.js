const _ = require("lodash");
const fs = require('fs');
const os = require('os');
const XLSX = require('xlsx');
// var xlsx_style = require('xlsx-style');

/**
 * 模板方法
 */
exports.readExcel = function (params, config) {
  console.log('excel操作');
  console.log(params);
  const promise = new Promise((resolve, reject) => {
    if (params.path) {
      var workbook = XLSX.readFile(params.path);
      console.log('====== workbook ======');
      console.log(workbook.Sheets[workbook.SheetNames[0]]);
      console.log('====== workbook ======');
      var sheets = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], {
        header: 1
      });
      // let enter = os.platform === 'win32' ? '\n\r' : '\n';
      // let path = `${os.homedir()}/.uiauto/${config.project_name}/.uiauto.log`;
      // console.log(sheets);
      // console.log(_.chunk(_.flatten(sheets), 5).join('\n'));
      // console.log('写入log文件');
      // fs.writeFileSync(path, _.chunk(_.flatten(sheets), 5).join('\n') + enter, {
      //   encoding: 'utf8',
      //   flag: 'a'
      // })
      // var formulae = XLSX.utils.sheet_to_formulae(workbook.Sheets[workbook.SheetNames[0]]);
      // console.log(formulae);
      console.log('====sheets=====');
      console.log(sheets);

      resolve(sheets);
    } else {
      reject();
    }
  })
  return promise;
};

exports.writeExcel = function (params) {
  console.log(params.data);
  const promise = new Promise((resolve, reject) => {
    try {
      const filePath = params.path + '/' + params.name;

      if (typeof (params.data) != 'object') {
        params.data = JSON.parse(params.data);
      }

      if (!params.data) {
        params.data = [];
      }

      let uniq_column_index = -1;
      if (params.headers && params.uniq_column) {
        uniq_column_index = params.headers[0].indexOf(params.uniq_column);
      }

      let items = [];
      let new_items = [];
      if (params['write_type'] === "append") {
        if (fs.existsSync(filePath)) {
          const workbook = XLSX.readFile(filePath);
          const sheets = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], {
            header: 1
          });

          if (sheets && sheets.length > 1) {
            items = _.slice(sheets, 1, sheets.length);

            if (items && items.length > 0) {
              if (!!params['uniq_column'] && uniq_column_index > -1) {
                _.forEach(params.data, (data) => {

                  _.remove(items, (item) => {
                    const result = item.indexOf(data[uniq_column_index]) > -1;

                    return result;
                  });

                });
              }

              items = _.concat(items, params.data);
            } else {
              items = params.data;
            }
          } else {
            items = params.data;
          }
        } else {
          items = params.data;
        }
      } else {
        items = params.data;
      }

      if (params.headers) {
        items = _.concat(params.headers, items);
      }

      var output = XLSX.utils.json_to_sheet(items, {
        skipHeader: true
      });

      console.log('======= writeexcel output =======');
      console.log(output);

      // var headerChar = _.keys(_.extend.apply(this, params.data));
      // var refEnd = headerChar[headerChar.length - 1] + (params.data.length + 1);
      var wb = {
        SheetNames: ['mySheet'],
        Sheets: {
          'mySheet': Object.assign({}, output)
        }
      };

      XLSX.writeFile(wb, filePath);
      resolve();
    } catch (e) {
      reject(e.stack);
    }
  });
  return promise;
};


exports.getDataFromExcel = function (params) {
  console.log('===== getDataFromExcel params =====');
  console.log(params);
  // if (params.path) {
  // var workbook = XLSX.readFile(params.path);
  console.log('====== getDataFromExcel ======');
  // var charpart = params.cellAddress.substr(0, 1);
  // var numpart = params.cellAddress.substr(1) - 1;
  // var charToNum = charpart.charCodeAt() - 65;
  // console.log(params.data[numpart][charToNum]);
  // return params['data'][numpart][charToNum];

  const promise = new Promise((resolve, reject) => {
    if (params.path) {
      var workbook = XLSX.readFile(params.path);
      /* Get worksheet */
      var worksheet = workbook.Sheets[workbook.SheetNames[0]];
      var formula = params.formula;
      console.log('====== formula =======');
      console.log(formula);
      console.log('====== formula =======');
      var cellLs = formula.match(/[A-Z]+[1-9]+[0-9]*/g);
      console.log(cellLs);
      console.log('===-=-=-=-=-=');
      var replaceLs = {};
      if (formula.match(/^[A-Z]+[1-9]+[0-9]*$/)) {
        resolve(getValueByCellAddress(worksheet, formula));
      } else {
        _.each(cellLs, function (item) {
          replaceLs[item] = getValueByCellAddress(worksheet, item);
        })
        _.each(replaceLs, function (val, key) {
          formula = formula.replace(RegExp(key, "g"), val);
          console.log(formula);
          console.log('===-=-= in each -=-=-=');
        })

        console.log(replaceLs);
        console.log('===-=-=-=-=-=');
        try {
          console.log('----result -----');
          var result = eval(formula);
          resolve(result);
        } catch (err) {
          reject('单元格地址或算式格式错误');
        }
      }
    }
  })
  return promise;
};

var getValueByCellAddress = function (worksheet, cellAddress) {
  /* Find desired cell */
  var desired_cell = worksheet[cellAddress];
  /* Get the value */
  return desired_cell ? (desired_cell.w ? desired_cell.w : (desired_cell.v ? desired_cell.v : undefined)) : undefined;
}
