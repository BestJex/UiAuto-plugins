const _ = require("lodash");
var util = require("./util.js");
const cheerio = require('cheerio');
const fs = require('fs');

/**
 * 根据html解析元素
 */
exports.getHtmlElements = function (params) {
  return new Promise((resolve, reject) => {
    const html = params.html;
    const selector = params.selector;
    const context = params.context;
    const method = params.method || 'html';
    let range = getRange(params.range);
    let results = [];

    // const html = fs.readFileSync('./test.html', 'utf8');
    // const selector = '.result.c-container h3 a';
    const $ = cheerio.load(html, { decodeEntities: false });

    console.log('------------------------------------------------')
    console.error(range)

    $(selector, context).map((i, el) => {
      console.log(!range);
      console.log(!range.length);
      console.log(i + 1)
      console.log(!range.includes(i + 1) && !range.includes(i + 1 + ''))
      if (!range || !range.length || (!range.includes(i + 1) && !range.includes(i + 1 + ''))) return;

      results.push($(el)[method]());
    });

    console.log(results)
    resolve(results);
  })

}

function getRange(range) {
  if (range) {
    range = String(range).split(',');
    _.each(range, (set, si) => {
      console.warn(set)
      if (set.includes('~')) {
        let range_set = set.split('~');
        let gap = Math.abs(_.head(range_set) - _.tail(range_set)) + 1;
        console.warn(range_set)
        let min = Math.min.apply(this, range_set);

        range[si] = _.map(Array(gap), (item, gi) => {
          return gi + min + '';
        })
      }
    })

    range = _.compact(_.uniq(_.flattenDeep(range)));
    return range
  } else return null;
}

/* getHtmlElement({
  html: '<xxx></xxx>',
  selector: '.class',
  context: '.parentClass',
  method: 'text'
}) */