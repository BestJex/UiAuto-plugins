"""
by suyj
"""
from pykeyboard import PyKeyboard
import time
import sys
import json
import base64
import uiautomation as uiauto
from browser import ResumeBrowser, generate_xpath
import traceback

keyboard = PyKeyboard()


def press_down():
    keyboard.tap_key(keyboard.down_key)


def press_up():
    keyboard.tap_key(keyboard.up_key)


def press_tab():
    keyboard.tap_key(keyboard.tab_key)


def send_keys(str_, interval=.2):
    keyboard.type_string(str_, interval)


def press_esc():
    keyboard.tap_key(keyboard.escape_key)


def press_left():
    keyboard.tap_key(keyboard.left_key)


def press_right():
    keyboard.tap_key(keyboard.right_key)


def backspace():
    keyboard.tap_key(keyboard.backspace_key)


def press_enter():
    keyboard.tap_key(keyboard.enter_key)


def press_space():
    keyboard.tap_key(keyboard.space_key)


def user_input(params):
    operations = params['operation']
    operate_times = int(params.get('operate_times', 1))
    interval = float(params.get('interval'))
    if operations == 'send_keys':
        input_text = params['input_text']
        eval('send_keys(input_text, interval)')
    else:
        for _ in range(operate_times):
            eval('{}()'.format(operations))
            time.sleep(interval)
    return


def key_send(params):
    try:

        element = None
        xpath = None
        if params['element_type'] == 'Xpath':
            if 'browser_info' in params.keys() and params['browser_info'] is not None:
                driver = params['browser_info']
                element = driver.find_element_by_xpath(params['element_xpath'])
                if element is not None:
                    element.clear()
                    element.send_keys(params['content'])
            else:
                raise Exception("缺少参数：浏览器对象")
        elif params['element_type'] == 'Browser':
            if params['browser_info'] is None or params['browser_info'] == '':
                raise Exception("缺少参数：浏览器对象")
            if params['target_browser_element'] is None or params['target_browser_element'] == '':
                raise Exception("缺少参数：选择目标")

            target_element = params['target_browser_element']
            driver = params['browser_info']
            html = target_element['html']
            element = driver.find_element_by_xpath(html['xpath'])
            if element is not None:
                element.clear()
                element.send_keys(params['content'])
            else:
                raise Exception('元素不存在')
        elif params['element_type'] == 'Native':
            if params['target_native_element'] is None or params['target_native_element'] == '' or 'wnd' not in params['target_native_element'].keys():
                raise Exception("缺少参数：选择目标")

            wnd = params['target_native_element']['wnd']
            if "control_type_name" not in wnd.keys() or wnd["control_type_name"] is None or wnd["control_type_name"] == "":
                raise Exception(message="客户端界面元素没有control_type_name")
            elif "name" not in wnd.keys() or wnd["name"] is None or wnd["name"] == "":
                raise Exception(message="客户端界面元素没有name属性")
            elif hasattr(uiauto, wnd["control_type_name"]) is False:
                raise Exception(message="uiautomation不支持目标元素类型")
            else:
                control = getattr(uiauto, wnd["control_type_name"])(Name=wnd['name'])
                if hasattr(control, "SendKeys"):
                    control.SendKeys(params['content'])
        else:
            raise Exception(message="尚未支持当前元素的操作")

        return None
    except Exception as e:
        raise e
