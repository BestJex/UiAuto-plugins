import json
import os
import base64
import requests


def get_token(api_key='', secret_ket=''):
    # https://ai.baidu.com/docs#/Auth/75d80ed1
    # https://console.bce.baidu.com/ai/?_=1566193744774&fromai=1#/ai/ocr/app/detail~appId=289184
    # token示例： 24.ab1d7952f58aaed2a3cde0028e973037.2592000.1566550381.282335-16875827
    # 百度ocr技术提供相关api支持
    token_url = f'https://aip.baidubce.com/oauth/2.0/token?' \
                f'grant_type=client_credentials&client_id={api_key}&client_secret={secret_ket}&'
    res = requests.post(token_url)
    if 'access_token' in res.json().keys():
        access_token = res.json()['access_token']
        return access_token
    else:
        return 'error'


def encode_image(path):
    # 大于4M的图片无效
    try:
        size = os.path.getsize(path)
        if size > 4194304:
            return "image size too big"
        f = open(path, 'rb')
        # 进行图像base64编码
        img = base64.b64encode(f.read())
        return img
    except Exception as error:
        return error


def general_ocr(params):
    access_token = get_token(params['api_key'], params['secret_ket'])
    if 'error' not in access_token:
        url = 'https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic?access_token=' + access_token
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        if params['local_path']:
            data = {
                'image': encode_image(params['local_path'])
            }
        else:
            data = {
                'url': params['url']
            }
        res = requests.post(url=url, headers=headers, data=data)
        return json.dumps(json.loads(res.text), ensure_ascii=False)
    else:
        return access_token
